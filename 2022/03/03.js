const fs = require('fs');

let filename = "03_input.txt";

function getPriorities(doublette) {
    let code = doublette.charCodeAt(0);

    if (code > 96) {
        return code - 97 + 1;
    } else {
        return code - 65 + 27;
    }
}

fs.readFile(filename, 'utf8', (err, data) => {
    let sum = 0;
    let count = 0;
    let group = [];
    let badgeSum = 0;
    data.split(/\r?\n/).forEach(line =>  {
        let firstCompartnment = line.split("").slice(0, line.length/2)
        let secondCompartnment = line.split("").slice(line.length/2, line.length)

        let doublettes = firstCompartnment.filter((item) => {
            return secondCompartnment.includes(item);
        })

      //  console.log(firstCompartnment, secondCompartnment);
        let itemsPriority = getPriorities(doublettes[0]);
        sum += itemsPriority;

        group[count] = line.split("");
        if(count === 2) {
            let badges = group[0].filter((item)  => {
                return group[1].filter((item) => {
                    return group[2].includes(item);
                }).includes(item);
            })
            badgeSum += getPriorities(badges[0]);
            count = 0;
        } else {
            count++;
        }
    });
    console.log(badgeSum, sum);

});