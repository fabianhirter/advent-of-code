const fs = require('fs');

let filename = "04_input.txt";

function getRange(fromTo) {
    let from = parseInt(fromTo[0]);
    let to = parseInt(fromTo[1]);
    let output = [];
    for(let i = from; i<=to;i++) {
        output.push(i);
    }
    return output;
}

function contains(firstElve, secondElve) {
    let first = firstElve[0] >= secondElve[0];
    let second = firstElve[1] <= secondElve[1];
    return first && second;
}

function overlaps(firstElve, secondElve) {
    return firstElve[1] >= secondElve[0];
}

fs.readFile(filename, 'utf8', (err, data) => {
    let containCount = 0;
    let overlapCount = 0;
    data.split(/\r?\n/).forEach(line =>  {
        let firstElve, secondElve;
        [firstElve, secondElve] = line.split(',');
        firstElve = firstElve.split('-').map((item) => {
            return parseInt(item, 10);
        });
        secondElve = secondElve.split('-').map((item) => {
            return parseInt(item, 10);
        });

        if(contains(firstElve, secondElve) || contains(secondElve, firstElve)) {
            containCount++;
        }

        let firstRange = getRange(firstElve);
        let secondRange = getRange(secondElve);

        const overlap =  firstRange.some(item => {
            return secondRange.includes(item)
        })
        if(overlap) {
            overlapCount++
        }

    });
    console.log(containCount, overlapCount);
});