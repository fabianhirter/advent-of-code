const fs = require('fs');
const readline = require('readline');

const filename = "01_input.txt";

fs.readFile(filename, 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }

    let sum = 0;
    let sums = [];
    data.split(/\r?\n/).forEach(line =>  {
        if(line === "") {
            sums.push(sum);
            sum = 0;
        } else {
            sum += parseInt(line);
        }
    });
    sums.sort(function(a, b){return b - a});
    console.log(sums[0]+sums[1]+sums[2]);
});