const fs = require('fs');

function getWinScore(a, b) {
    let aChar = a.charCodeAt(0);
    let bChar = b.charCodeAt(0)-23;

    if(aChar === bChar) {
        return 3;
    }

    if((bChar - 1 === aChar) || (bChar === aChar - 2)) {
        return 6;
    }
    return 0
}

function getPlayScore(a) {
    return a.charCodeAt(0) - 87;
}

function getNeededPlay(play, score) {
    // win
    if(score == "Z") {
        let code = play.charCodeAt(0) + 1 + 23;
        if(code > 90) {
            code = code - 2;
        }
        return String.fromCharCode(code);
    }

    // draw
    if(score == "Y") {
        return String.fromCharCode(play.charCodeAt(0) + 23);
    }

    // loose
    if(score == "X") {
        let code = play.charCodeAt(0) + 2 + 23;
        if(code > 90) {
            code = code - 2;
        }
        return String.fromCharCode(code);
        switch (play) {
            case 'A':
                return 'Z';
            case 'B':
                return 'X';
            case 'C':
                return 'Y';
        }
    }
}

let filename = "02_input.txt";
fs.readFile(filename, 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }

    let score = 0;
    data.split(/\r?\n/).forEach(line =>  {
        let a = line.charAt(0);
        let b = line.charAt(2);
        let play = getNeededPlay(a, b)
        let winScore = getWinScore(a,play);
        let playScore = getPlayScore(play);
        score += winScore + playScore;
    });
    console.log(score);
});


/*
let x = getPlayScore('X');
let y = getPlayScore('Y');
let z = getPlayScore('Z');

let ax = getWinScore('A', 'X');
let ay = getWinScore('A', 'Y');
let az = getWinScore('A', 'Z');

let bx = getWinScore('B', 'X');
let by = getWinScore('B', 'Y');
let bz = getWinScore('B', 'Z');

let cx = getWinScore('C', 'X');
let cy = getWinScore('C', 'Y');
let cz = getWinScore('C', 'Z');


console.log(x,y,z);

console.log(ax,ay,az);
console.log(bx,by,bz);
console.log(cx,cy,cz);

*/